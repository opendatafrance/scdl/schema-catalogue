# Catalogue simplifié

Spécification du modèle de données relatif au catalogue des jeux de données publiés en open data par une collectivité

- nom : `catalogue`
- page d'accueil : https://gitlab.com/opendatafrance/scdl/catalogue
- URL du schéma : https://gitlab.com/opendatafrance/scdl/catalogue/raw/v0.1.2/schema.json
- version : `0.1.3`
- date de création : 20/11/2018
- date de dernière modification : 28/09/2023
- valeurs manquantes représentées par : `[""]`
- contributeurs :
  - OpenDataFrance (auteur)
  - Thomas Bekkers (contributeur)
  - Amélie Rondot (contributrice)

## Modèle de données

Ce modèle de données repose sur les 20 champs suivants correspondant aux colonnes du fichier tabulaire.

### `COLL_NOM`

- titre : Nom de la collectivité
- description : Nom officiel de la collectivité qui publie le catalogue simplifié de jeux de données limité à 140 caractères maximum.
- type : chaîne de caractères
- exemple : `Rennes Métropole`
- valeur obligatoire

### `COLL_SIRET`

- titre : Code SIRET de la collectivité
- description : Identifiant du [Système d'Identification du Répertoire des Etablissements](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements) (SIRET) de la collectivité qui publie le catalogue simplifié de jeux de données, composé de 9 chiffres SIREN + 5 chiffres NIC d’un seul tenant.
- type : chaîne de caractères
- exemple : `24350013900189`
- valeur obligatoire
- motif : `^\d{14}$`

### `ID`

- titre : Identifiant du jeu de donnée
- description : Cet identifiant est une chaîne de caractères qui correspond soit au nom du jeu de données, exprimé en minuscules sans accent ni espace (identifiant texte ou 'slug'), soit à un code d'identification généré automatiquement (identifiant machine ou 'hash').
- type : chaîne de caractères
- exemple : `prenoms-des-nouveaux-nes-a-rennes-en-2017 ou 53699116a3a729239d203b7f`
- valeur obligatoire

### `TITRE`

- titre : Titre du jeu de données
- description : Ce titre doit être un intitulé caractéristique et univoque permettant de désigner le jeu de données. Il est recommandé d'y faire figurer une indication de la zone géographique couverte et, lorsqu'elle se justifie, une indication de version ou de millésime.
- type : chaîne de caractères
- exemple : `Prénoms des nouveaux-nés à Rennes en 2017`
- valeur obligatoire

### `DESCRIPTION`

- titre : Description du jeu de données
- description : Cette description doit fournir un bref résumé narratif du contenu du jeu de données, rédigé de façon compréhensible pour l’utilisateur.
- type : chaîne de caractères
- exemple : `Liste annuelle des prénoms des nouveaux-nés enregistrés à l'état-civil de la ville de Rennes pour l'année 2017.`
- valeur obligatoire

### `THEME`

- titre : Thème du jeu de données
- description : En l'absence d'une nomenclature de classement par thèmes satisfaisante et adaptée au contexte local, le thème est exprimé sous la forme d'une chaîne de caractères libre dans la limite de 140 caractères maximum. Le manque de pertinence du [thésaurus EuroVoc](https://publications.europa.eu/fr/web/eu-vocabularies/th-dataset/-/resource/dataset/eurovoc) ou des [thèmes INSPIRE](https://inspire.ec.europa.eu/Themes/Data%20Specifications/2892) implique d'élaborer collectivement une [nomenclature spécifique](https://docs.google.com/document/d/1oDJsHw3bmABfto6HPgCPG1ztrV3CihuHjcfU8tQpvPc/) à partir d'un appariement des termes les plus utilisés sur les plateformes territoriales de données ouvertes.
- type : chaîne de caractères
- exemple : `Administration locale`
- valeur obligatoire

### `PRODUCTEUR_NOM`

- titre : Nom du producteur
- description : Nom officiel ou raison sociale du producteur du jeu de données limité à 140 caractères maximum.
- type : chaîne de caractères
- exemple : `Ville de Rennes`
- valeur obligatoire

### `PRODUCTEUR_SIRET`

- titre : Code SIRET du producteur
- description : Identifiant du [Système d'Identification du Répertoire des Etablissements](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements) (SIRET) du producteur du jeu de données, composé de 9 chiffres SIREN + 5 chiffres NIC d’un seul tenant.
- type : chaîne de caractères
- exemple : `21350238800019`
- valeur obligatoire
- motif : `^\d{14}$`

### `COUV_SPAT_MAILLE`

- titre : Maille de couverture spatiale
- description : La maille de couverture spatiale correspond à l'echelle territoriale que couvre le jeu de données. Pour simplifier le renseignement de ce champ, elle est désignée en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Infracommunale', 'Communale', 'Intercommunale', 'Cantonale', 'Départementale', 'Régionale' ou 'Autre'.
- type : chaîne de caractères
- exemple : `Communale`
- valeur obligatoire
- valeurs autorisées : `["Infracommunale","Communale","Intercommunale","Cantonale","Départementale","Régionale","Autre"]`

### `COUV_SPAT_NOM`

- titre : Nom de couverture spatiale
- description : Le nom de couverture spatiale correspond au nom de l'échelle territoriale que couvre le jeu de données. Il est exprimé sous la forme d'une chaîne de caractères limitée à 140 caractères maximum.
- type : chaîne de caractères
- exemple : `Rennes`
- valeur obligatoire

### `COUV_TEMP_DEBUT`

- titre : Date de début de la couverture temporelle
- description : La couverture temporelle correspond à la période que couvre le jeu de données. Cette période est un intervalle entre deux dates. La date de début est donc le premier terme utilisé pour désigner cet intervalle, exprimé au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2017-01-01`
- valeur obligatoire

### `COUV_TEMP_FIN`

- titre : Date de fin de la couverture temporelle
- description : La couverture temporelle correspond à la période que couvre le jeu de données. Cette période est un intervalle entre deux dates. La date de fin est donc le second terme utilisé pour désigner cet intervalle, exprimé au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2017-12-31`
- valeur obligatoire

### `DATE_PUBL`

- titre : Date de la première publication
- description : Date de la publication initiale du contenu du jeu de données. Elle est exprimée au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2017-06-01`
- valeur obligatoire

### `FREQ_MAJ`

- titre : Fréquence de la mise à jour
- description : La fréquence de mise à jour correspond à la périodicité suivant laquelle des modifications sont apportées au jeu de données. Pour simplifier le renseignement de ce champ, elle est désignée en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Inconnue', 'Ponctuelle', 'Irrégulière', 'Continuelle', 'Toutes les heures', 'Quotidienne ou plusieurs fois par jour', 'Hebdomadaire ou plusieurs fois par semaine', 'Mensuelle ou plusieurs fois par mois', 'Bimestrielle', 'Trimestrielle', 'Semestrielle', 'Annuelle', 'Biennale', 'Triennale', ou 'Quinquennale'.
- type : chaîne de caractères
- exemple : `Semestrielle`
- valeur obligatoire
- valeurs autorisées : `["Inconnue","Ponctuelle","Irrégulière","Continuelle","Toutes les heures","Quotidienne ou plusieurs fois par jour","Hebdomadaire ou plusieurs fois par semaine","Mensuelle ou plusieurs fois par mois","Bimestrielle","Trimestrielle","Semestrielle","Annuelle","Biennale","Triennale","Quinquennale"]`

### `DATE_MAJ`

- titre : Date de la dernière mise à jour
- description : Date de la dernière modification effective du contenu du jeu de données. Elle est exprimée au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2018-01-14`
- valeur obligatoire

### `MOTS_CLES`

- titre : Mots clés
- description : Un ou plusieurs mot(s) clé(s) utilisé(s) pour décrire le jeu de données en minuscules non accentuées. S'il y en a plusieurs, le séparateur est le point-virgule.
- type : chaîne de caractères
- exemple : `scdl;prenoms;etat-civil`
- valeur obligatoire

### `LICENCE`

- titre : Licence appliquée sur le jeu de données
- description : Désignation de la licence qui encadre la réutilisation du jeu de données. En France, le [décret n° 2017-638 du 27 avril 2017](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000034502557) restreint le choix exclusivement à deux licences. D'autres sont néanmoins utilisées par quelques producteurs ou acteurs territoriaux pour encadrer la réutilisation de certains jeux de données. Pour simplifier le renseignement de ce champ, la licence du jeu de données est désignée en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Licence Ouverte-LO', 'Open Database License-ODBL', 'Creative Commons-CC', 'Spécifique ou autre'.
- type : chaîne de caractères
- exemple : `Licence Ouverte-LO`
- valeur obligatoire
- valeurs autorisées : `["Licence Ouverte-LO","Open Database License-ODBL","Creative Commons-CC","Spécifique ou autre"]`

### `NOMBRE_RESSOURCES`

- titre : Nombre de ressource(s)
- description : Nombre de ressource(s) mise(s) à disposition dans le jeu de données.
- type : nombre entier
- exemple : `3`
- valeur obligatoire
- valeur minimale : 1

### `FORMAT_RESSOURCES`

- titre : Format(s) des ressources
- description : Format(s) dans le(s)quel(s) la (ou les) ressource(s) du jeu de données est (sont) mise(s) à disposition. Ce(s) format(s) est (sont) exprimé(s) en minuscules non accentuées. S'il y en a plusieurs, le séparateur est le point-virgule.
- type : chaîne de caractères
- exemple : `csv;xls;json`
- valeur obligatoire

### `URL`

- titre : URL d'accès
- description : Cet élément fournit un lien, une adresse web la plus stable possible, vers la page du jeu de données (ou de la ressource si le jeu de données n'en comprend qu'une) et/ou vers des informations complémentaires le concernant.
- type : chaîne de caractères (uri)
- exemple : `https://data.rennesmetropole.fr/explore/dataset/prenoms-des-nouveaux-nes-a-rennes-en-2017`
- valeur obligatoire
